module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  css: {
    loaderOptions: {
      sass:{
        prependData: "@import 'src/scss/variables'"
      },
      scss: {
        prependData: "@import 'src/scss/variables';"
      }
    }
  }
}
