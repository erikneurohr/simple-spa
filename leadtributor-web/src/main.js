import Vue from 'vue'
import VueCompositionApi from '@vue/composition-api'
import App from './App.vue'
import router from './router'
// import store from './store'

import localization from '@/plugins/localization'
import vuetify from './plugins/vuetify';
import VueAxios from 'vue-axios'
import axios from 'axios'

axios.defaults.baseURL = "https://c2ibusyi5k.execute-api.eu-central-1.amazonaws.com/default"

Vue.config.productionTip = false
Vue.use(VueCompositionApi)
Vue.use(localization)
Vue.use(VueAxios, axios)

new Vue({
  router,
  // store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
