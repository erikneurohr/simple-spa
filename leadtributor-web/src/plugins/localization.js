import { ref, provide, inject } from '@vue/composition-api'

// eslint-disable-next-line symbol-description
const localizationDataSymbol = Symbol()

// eslint-disable-next-line symbol-description
const localizationSymbol = Symbol()

function provideLocalizationData (languages) {
  const locale = process.env.VUE_APP_LOCALE || 'de'
  const localizationData = {
    locale: ref(locale),
    languages: languages
  }
  provide(localizationDataSymbol, localizationData)
}

export function injectLocalizationData () {
  const localizationData = inject(localizationDataSymbol)
  if (!localizationData) throw new Error('No localization data provided')
  return localizationData
}

function $t (key) {
  const { locale, languages } = injectLocalizationData()
  try {
    return key.split('.').reduce((accumulator, currentValue) => {
      if (accumulator) return accumulator[currentValue]
    }, languages[locale.value])
  } catch (_) {
    return ''
  }

}

export function provideLocalization (languages) {
  provideLocalizationData(languages)
  provide(localizationSymbol, $t)
}

export function injectLocalization () {
  const localization = inject(localizationSymbol)
  if (!localization) throw new Error('No localization provided')
  return localization
}

function install (Vue) {
  Vue.prototype.$t = $t
}

export default {
  install
}
