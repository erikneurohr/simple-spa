import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginView from "@/views/auth/LoginView";
import Home from '@/views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: LoginView,
    beforeEnter : (to, from, next) => {
      const loggedIn = localStorage.getItem("idToken");
      if(loggedIn) {
        return next("/");
      }
      next();
    }
  },
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  const loggedIn = localStorage.getItem("idToken");
  const publicRes = ['/login']
  if(!publicRes.includes(to.path) && !loggedIn) {
    return next('/login');
  }
  next();
});

export default router
