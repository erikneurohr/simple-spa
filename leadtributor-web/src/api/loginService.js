import Vue from 'vue'

export default class LoginService {

  login (username, password) {
    return Vue.axios.post('user/login',
      {
        "username": username,
        "password": password
      })
  }

  getCurrentUser() {
    return localStorage.getItem("idToken");
  }

  isLoggedIn() {
    return localStorage.getItem("idToken") != null;
  }

}
