import React from 'react';
import './App.css';
import Frame from "./ui/frame/Frame";

import Amplify from 'aws-amplify'
import awsconfig from './aws-exports'
import {UserProvider} from "./auth/user";
import {AmplifyAuthenticator, AmplifySignIn, AmplifySignUp} from "@aws-amplify/ui-react";

Amplify.configure(awsconfig)

export default function AppWithAuth() {

        return (
                <UserProvider>
                    <AmplifyAuthenticator >
                        <AmplifySignIn slot="sign-in"/>
                        <AmplifySignUp
                            formFields={[
                                { type: 'username', label: 'Username', placeholder: 'Enter Your Username', hint: 'Username', required: true },
                                { type: 'email', label: 'Email', placeholder: 'Email', hint: 'Enter Your Email', required: true },
                                { type: 'password', label: 'Password', placeholder: 'Enter Your Password', hint: 'Password', required: true },
                                { type: 'address', label: 'Address', placeholder: 'Enter Your Address', hint: 'Address', required: true },
                                { type: 'given_name', label: 'Given Name', placeholder: 'Enter Your Given Name', hint: 'given name', required: true },
                                { type: 'family_name', label: 'Family Name', placeholder: 'Enter Your Family Name', hint: 'family name', required: true },
                            ]}
                            slot="sign-up"/>
                        <div>
                            <Frame />
                        </div>
                    </AmplifyAuthenticator>
                </UserProvider>
        );
}
