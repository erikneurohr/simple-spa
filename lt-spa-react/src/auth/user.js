import React from "react";
import {Auth} from "aws-amplify";

const UserContext = React.createContext(null);

export const UserProvider = (props) => {
    const [user, setUser] = React.useState(null);
    // fetch the info of the user that may be already logged in
    React.useEffect(() => {
        Auth.currentAuthenticatedUser()
            .then(user => setUser(user))
            .catch(() => setUser(null));
    }, []);
    const login = (usernameOrEmail, password) =>
        Auth.signIn(usernameOrEmail, password)
            .then(cognitoUser => setUser(cognitoUser));
    const logout = () =>
        Auth.signOut()
            .then(() => setUser(null));
    return (
        <UserContext.Provider value={{user, login, logout}}>
            {props.children}
        </UserContext.Provider>
    )
}

export const useUser = () => {
    const context = React.useContext(UserContext);
    if (context === undefined) {
        throw new Error('`useUser` must be within a `UserProvider`');
    }
    return context;
};
