import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import {useUser} from '../../auth/user'
import {Greetings} from "aws-amplify-react";
import CustomGreetings from "../../auth/CustomGreetings";
import {AmplifySignOut} from "@aws-amplify/ui-react";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function LTAppBar() {
    const classes = useStyles();
    const {user, logout} = useUser();
    return (
        <AppBar position="static">
            <Toolbar>
                <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                    <MenuIcon/>
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                    <CustomGreetings />
                </Typography>
                <Button color="inherit" onClick={() => {logout();window.location.reload();}}>Logout</Button>
            </Toolbar>
        </AppBar>
    );


}


