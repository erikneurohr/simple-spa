import * as React from "react";
import {useCallback, useState} from "react";
import LTAppBar from "./LTAppBar";
import {API} from "@aws-amplify/api";
import Button from "@material-ui/core/Button";
import {Card, CardContent} from "@material-ui/core";


function useApiData(initialState = null) {

    const [apiData, setApiData] = useState(initialState);

    const reset = useCallback(() => {
        setApiData(initialState);
    }, [initialState]);

    return {
        apiData,
        setApiData,
        reset
    }
}

export default function Frame() {
    const {apiData, setApiData, reset} = useApiData();

    const handleClick = async () => {
        const data = await API.get("leadsAPI", "/leads/urn:lt:leads:lead:c978d242-f6e2-4abb-92ae-b4e57624b7b3", {});
        setApiData(data)
    }

    const handleClick2 = async () => {
        const xhr = new XMLHttpRequest()

        // get a callback when the server responds
        xhr.addEventListener('load', () => {
            // update the state of the component with the result here
            console.log(xhr.responseText)
        })
        // open the request with the verb and the url
        xhr.open('POST', 'https://81tsnei9ze.execute-api.eu-central-1.amazonaws.com/user/signup')
        // send the request
        xhr.send(JSON.stringify({
            email: 'aws-dev@leadtributor.net',
            password : 'Test12345678$'
        }))
    }
    const handleClick3 = async () => {
        const xhr = new XMLHttpRequest()

        // get a callback when the server responds
        xhr.addEventListener('load', () => {
            // update the state of the component with the result here
            console.log(xhr.responseText)
        })
        // open the request with the verb and the url
        xhr.open('POST', 'https://81tsnei9ze.execute-api.eu-central-1.amazonaws.com/user/login')
        xhr.withCredentials = true;
        // send the request
        xhr.send(JSON.stringify({
            username: 'aws-dev@leadtributor.net',
            password : 'Test12345678$'
        }))
    }

    return (
        <div>
            <LTAppBar/>
            <div style={{"padding-top": "10px"}}>

                <Card variant="outlined">
                    <CardContent>
                        {
                            apiData != null ?
                                <pre>{JSON.stringify(apiData, null, 4)}</pre>
                                : null
                        }
                        <Button color="primary" variant="contained" onClick={handleClick}>Get Lead</Button>
                    </CardContent>
                </Card>

                <Card variant="outlined">
                    <CardContent>
                        <Button color="primary" variant="contained" onClick={handleClick2}>Register</Button>
                    </CardContent>
                </Card>


                <Card variant="outlined">
                    <CardContent>
                        <Button color="primary" variant="contained" onClick={handleClick3}>Login</Button>
                    </CardContent>
                </Card>

            </div>
        </div>
    )
}
