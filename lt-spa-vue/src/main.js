import Vue from 'vue'
import App from './App.vue'

import VueFlex from "vue-flex";
import "vue-flex/dist/vue-flex.css";

import Amplify from 'aws-amplify';
import aws_exports from './aws-exports';

import VueRouter from "vue-router";
import router from './router'

Amplify.configure(aws_exports);

Vue.config.productionTip = false

Vue.use(VueRouter);
Vue.use(VueFlex);

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
