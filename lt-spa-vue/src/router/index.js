import VueRouter from 'vue-router'
import Welcome from "@/components/common/Welcome";
import Welcome2 from "@/components/common/Welcome2";

export default new VueRouter({
    routes : [
        {
            path : '/',
            name: 'Welcome',
            component: Welcome
        },
        {
            path : '/a',
            name: 'Welcome2',
            component: Welcome2
        }
    ]
})
