import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from '../views/welcome/welcome.component';
import {SecondviewComponent} from '../views/secondview/secondview.component';
import {AuthGuardGuard} from '../guards/auth-guard.guard';

const routes: Routes = [
  {path: '', component: WelcomeComponent, canActivate: [AuthGuardGuard]},
  {path: 'second', component: SecondviewComponent}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
    })
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
