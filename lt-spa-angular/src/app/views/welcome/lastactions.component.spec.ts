import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastactionsComponent } from './lastactions.component';

describe('LastactionsComponent', () => {
  let component: LastactionsComponent;
  let fixture: ComponentFixture<LastactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
