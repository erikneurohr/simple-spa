import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomeComponent } from './secondview.component';

describe('WelcomeComponent', () => {
  let component: SecondviewComponent;
  let fixture: ComponentFixture<SecondviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
