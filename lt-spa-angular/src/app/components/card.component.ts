import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input()
  title: string;

  @Input()
  justifyHeaderContent: string;

  constructor() { }

  ngOnInit(): void {
    console.log(this.title);
    console.log(this.justifyHeaderContent);
  }

}
