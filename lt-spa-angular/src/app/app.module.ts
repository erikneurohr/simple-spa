import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule, FlexModule, GridModule} from '@angular/flex-layout';
import {HeaderComponent} from './frame/header.component';
import {BrandComponent} from './frame/brand.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {SidenavComponent} from './frame/sidenav.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MainComponent} from './frame/main.component';
import {NewsComponent} from './views/welcome/news.component';
import {WelcomeComponent} from './views/welcome/welcome.component';
import {CenterComponent} from './views/welcome/center.component';
import {LastactionsComponent} from './views/welcome/lastactions.component';
import {CardComponent} from './components/card.component';
import {AppRoutingModule} from './app-routing/app-routing.module';
import {SecondviewComponent} from './views/secondview/secondview.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BrandComponent,
    SidenavComponent,
    MainComponent,
    NewsComponent,
    WelcomeComponent,
    CenterComponent,
    LastactionsComponent,
    CardComponent,
    SecondviewComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FlexModule,
    MatButtonToggleModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatSidenavModule,
    GridModule,
    FlexLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
